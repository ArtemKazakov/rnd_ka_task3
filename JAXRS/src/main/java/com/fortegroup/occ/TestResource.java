package com.fortegroup.occ;

import com.fortegroup.occ.model.Information;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(value = "/api")
public class TestResource {

    private final static Logger logger = Logger.getLogger(TestResource.class);

    @POST
    @Path(value = "/jsonHook")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response testJsonHook(String json) {
        logger.info(json);

        return Response.ok().build();
    }

    @POST
    @Path(value = "/json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response testJson(Information information) {
        logger.info(information.getText());

        return Response.ok().build();
    }

    @POST
    @Path(value = "/xml")
    @Consumes(MediaType.APPLICATION_XML)
    public Response testXML(Information information) {
        logger.info(information.getText());

        return Response.ok().build();
    }
}
