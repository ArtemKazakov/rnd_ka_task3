package com.fortegroup.occ;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.internal.util.Base64;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.StringTokenizer;

@Provider
public class SecurityFilter implements ContainerRequestFilter {

    private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
    private static final String AUTHORIZATION_HEADER_PREFIX = "Basic ";

    private static final String UNAUTHORIZED_MESSAGE = "User cannot access the resource.";

    private static final String LOGIN = "eugene";
    private static final String PASSWORD = "eugene";

    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        List<String> authHeader = containerRequestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
        if (authHeader != null && authHeader.size() > 0) {
            String authToken = authHeader.get(0);
            authToken = authToken.replaceFirst(AUTHORIZATION_HEADER_PREFIX, "");
            String decodedString = Base64.decodeAsString(authToken);
            StringTokenizer tokenizer = new StringTokenizer(decodedString, ":");
            String username = tokenizer.nextToken();
            String password = tokenizer.nextToken();

            if (username.equals(LOGIN) && password.equals(PASSWORD)){
                return;
            }
        }

        Response unauthorizedStatus = Response.status(Response.Status.UNAUTHORIZED)
                .entity(UNAUTHORIZED_MESSAGE).build();

        containerRequestContext.abortWith(unauthorizedStatus);
    }

}
